export const changelog = {
  'v1.0.0': {
    name: 'Start',
    changes: [
      'Added changelog page.',
      'Installed nuxtjs.',
      'Installed vuetify.',
      'Installed vuex.',
      'Installed vue.'
    ]
  }
}
