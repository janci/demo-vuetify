import { DECREMENT_ME, INCREMENT_ME } from '~/store/action-types'
import { changelog } from '~/store/static/changelog'

export const state = () => ({
  counter: 0,
  changelog
})

export const mutations = {
  increment (state) {
    state.counter++
  },
  decrement (state) {
    state.counter--
  }
}

export const actions = {
  [INCREMENT_ME] (context) {
    context.commit('increment')
  },
  [DECREMENT_ME] (context) {
    context.commit('decrement')
  }
}
